<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LayoutPageController extends Controller
{
    public function layout()
    {
        return view('layout');
    }
}
